import React, { Component } from 'react';

import { connect } from 'react-redux';

import {View, Text, Image, Keyboard,Alert,ScrollView,TouchableOpacity, ToastAndroid,AsyncStorage} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { BarIndicator} from 'react-native-indicators';
import {fbApp} from '../index'
import {styles,config} from '../config'
class Registration extends Component{
    constructor(){
        super();
        this.state={
            email:'',
            password:'',
            passwordChecking:'',
            keyboard:false,
            loading:false
        }
    }
    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    _keyboardDidShow = () => this.setState({keyboard:true});
    _keyboardDidHide = () => this.setState({keyboard:false});

    validateEmail() {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(this.state.email).toLowerCase());
    }
    validatePasswordVerification(){
        if(this.state.password!==this.state.passwordChecking)return false;
        return true;
    }
    validatePassword(){
        if(this.state.password.length<6) return false;
        return true;
    }
    onRegister = () => {
        if(!this.validateEmail())return Alert.alert('Error','Не правильно указан Email');
        if(!this.validatePasswordVerification())return Alert.alert('Error','Пароли не совпадат');
        if(!this.validatePassword())return Alert.alert('Error','Пароль должен быть больше 6 символов');
        this.setState({loading:true})
        const { email, password } = this.state;

        fbApp.auth().createUserWithEmailAndPassword(email, password)
            .then(async (user) => {
                let uri ='users/'+user.uid+'/profile';
                fbApp.database().ref(uri).set({email:user.email})
                fbApp.messaging().getToken().then(
                    (token) => {
                        fbApp.database().ref('users/'+user.uid+'/notificationTokens').push(
                            {token:token}
                        );
                    }
                );
                AsyncStorage.setItem('uid', user.uid);
                AsyncStorage.setItem('auth', 'true');
                Actions.Main();
                this.setState({loading:false})
        })
            .catch((error) => {
                const { code, message } = error;
                console.log(code,message)
                ToastAndroid.show('Error', ToastAndroid.SHORT)
                this.setState({loading:false})
            });
    }

    render() {
        return (
            <View style={{flex:1,padding:25, backgroundColor:config.backgroundAppColor}}>
                {this.state.loading?
                    <View style={styles.loadingView}>
                        <BarIndicator color='#279ef4' size={100}/>
                    </View> :null}
                      <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{alignItems:'center'}}>
                            <Image style={{height: 150, width:150}} source={require('../images/logo.png')}/>
                        </View>
                        <Form>
                            <Item>
                                <Input
                                    style={styles.IOSTextInput}
                                    autoFocus
                                    ref="email"
                                    returnKeyType='next'
                                    onSubmitEditing={() => this.refs.password._root.focus()}
                                    autoCapitalize="none"
                                    keyboardType="email-address"
                                    placeholder="Email"
                                    value={this.state.email}
                                    onChangeText={(text)=>this.setState({email:text})}
                                />
                            </Item>
                            <Item>
                                <Input
                                    style={styles.IOSTextInput}
                                    ref="password"
                                    returnKeyType='next'
                                    onSubmitEditing={() => this.refs.passwordVer._root.focus()}
                                    autoCapitalize="none"
                                    placeholder="Пароль*"
                                    secureTextEntry
                                    value={this.state.password}
                                    onChangeText={(text)=>this.setState({password:text})}
                                />
                            </Item>
                            <Item>
                                <Input
                                    style={styles.IOSTextInput}
                                    ref="passwordVer"
                                    returnKeyType='done'
                                    onSubmitEditing={() => this.onRegister()}
                                    autoCapitalize="none"
                                    placeholder="Подтвердите пароль*"
                                    secureTextEntry
                                    value={this.state.passwordChecking}
                                    onChangeText={(text)=>this.setState({passwordChecking:text})}
                                />
                            </Item>
                            <View style={{flexDirection:'row',justifyContent:'center'}}>
                                <Button style={styles.marginButton} block rounded onPress={()=>this.onRegister()}>
                                    <Text style={styles.textButtonColor}>Вход</Text>
                                </Button>
                            </View>
                        </Form>
                    </View>
            </View>
        );
    }

}

export default connect(
    state => ({
        state:state
    })
)(Registration);
