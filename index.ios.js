import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
    TouchableOpacity,
    Image,
    AsyncStorage,
    StatusBar
} from 'react-native';

//config
import {config,styles} from './config';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    CachedImage,            // react-native component that is a drop-in replacement for your react-native `Image` components
    ImageCacheProvider,     // a top level component that provides accsess to the underlying `ImageCacheManager` and preloads images
    ImageCacheManager
} from 'react-native-cached-image';

//scenes
import NewPayment from './components/Finance/NewPayment'
import Finance from './components/Finance/Finance';
import Notes from './components/Notes/Notes';
import Pills from './components/Pills';
import TheDoctors from './components/TheDoctors/TheDoctors';
import VisitsToTheDoctor from './components/VisitToTheDoctor/VisitsToTheDoctor';
import VisitTo from './components/VisitToTheDoctor/VisitTo';
import NewNote from './components/Notes/NewNote';
import NewDoctor from './components/TheDoctors/NewDoctor';
import Login from './components/Login';
import Registration from './components/Registration';
import Profile from './components/Profile';
import EditProfile from './components/EditProfile';
import ForgotPassword from './components/ForgotPassword';
import Notifications from './components/Notifications';
import AllFinance from './components/Finance/AllFinance';

//Reducer
import Reducer from './reducers/reducer';

//redux
import { createStore,applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Router, Scene, Actions,Tabs,Stack, ActionConst } from 'react-native-router-flux';
import { BarIndicator} from 'react-native-indicators';

//firebase
import firebase from 'react-native-firebase';
export const iosConfig = {
   //private info
};
export const fbApp = firebase.initializeApp(iosConfig, 'fbApp');

export const store = createStore(Reducer);
const TabIcon = ({ focused, title, iconName }) => {
    const icon = iconName=='visits'?require('./images/icons/visits.png'):
        iconName=='pills'?require('./images/icons/pills.png'):
            iconName=='notes'?require('./images/icons/notes.png'):
                iconName=='doctor'?require('./images/icons/doctor.png'):
                    iconName=='coins'?require('./images/icons/coins.png'):null;
    return (
        <View style={{borderBottomWidth: focused ? 1 : 0,
        borderBottomColor: focused ? 'white' : 'transparent',
        justifyContent:'center',alignItems:'center'}}>
            <Image style={{height: 28, width:28}} source={icon}/>
            <Text style={{fontSize:12,color:'white'}}>{title}</Text>
        </View>
    )
};

export default class App extends Component {
    constructor(){
        super();
        this.state={
            auth:false,
            loading:true
        }
    }
    _rightButtonClose(){
        return(
            <TouchableOpacity onPress={() =>  Actions.pop() } >
                <Icon name="md-close" size={30} style={{color:'#fff',marginRight:20}}/>
            </TouchableOpacity>
        )
    }
    _rightButton(){
        return(
            <TouchableOpacity onPress={() =>  Actions.Notifications() } >
                <Icon name="md-notifications" size={30} style={{color:'#fff',marginRight:20}}/>
            </TouchableOpacity>
        )
    }
    _leftButton(){
        return(
            <TouchableOpacity onPress={() => Actions.Profile()  } >
                <Icon name="ios-contact" size={30} style={{color:'#fff',padding:20}}/>
            </TouchableOpacity>
        )
    }
    async componentDidMount(){

        try {
            this.setState({loading:true})
            const value = await AsyncStorage.getItem('auth');
            if (value === null){
                this.setState({loading:false,auth:false})

            }else await this.setState({loading:false,auth:true})
        } catch (error) { this.setState({loading:false,auth:false})}
    }

  render() {
     return (
         this.state.loading?
             <View style={styles.loadingView}>
                 <View style={{alignItems:'center'}}>
                     <Image
                         style={{width: 200, height: 200}}
                         source={require('./images/kid.gif')}
                     />
                     <Text>Загрузка...</Text>
                 </View>
             </View> :
             <Provider store={store}>
                 <Router getSceneStyle={()=>({backgroundColor:'white'})}>
                     <Scene key="root" hideNavBar>
                         <Scene
                             initial={!this.state.auth}
                             type="reset"
                             key='login' title='Логин'
                             component={Login}
                             hideNavBar={false}
                             {...style}
                             titleStyle={style.stackTitle}
                         />
                         <Scene
                             key='registration' title='Регистрация'
                             {...style}
                             hideNavBar={false}
                             tintColor="#fff"
                             titleStyle={[style.stackTitle,{marginLeft:-30}]}
                             component={Registration}
                         />
                         <Scene
                             key='forgotpassword' title='Востановить пароль'
                             {...style}
                             hideNavBar={false}
                             tintColor="#fff"
                             titleStyle={[style.stackTitle,{marginLeft:-30}]}
                             component={ForgotPassword}
                         />
                         <Scene
                             initial={this.state.auth}
                             lazy={false}
                             key="Main" tabs={true} tabBarPosition={"bottom"} {...style}
                             type="reset"
                             showLabel={false}
                             swipeEnabled={false} activeTintColor="#ff9333" inactiveTintColor="#fff"
                         >
                             <Scene key="StackVisitsToTheDoctor" title="Визиты"
                                    iconName="visits"
                                    icon={TabIcon}
                             >
                                 <Scene key='VisitsToTheDoctor' title='Визиты к врачу'
                                        tabBarLabel="Визиты" component={VisitsToTheDoctor}
                                        cardStyle={{backgroundColor:'white'}}
                                        renderLeftButton={ ()=>this._leftButton()}
                                        titleStyle={style.stackTitle}
                                        renderRightButton={ ()=>this._rightButton()} />
                                 <Scene key='VisitTo' title='Визит к' component={VisitTo}
                                        tintColor="#fff"
                                        titleStyle={style.stackTitle}
                                        renderRightButton={ ()=>this._rightButton()}/>
                             </Scene>
                             <Scene key="StackNotes" title="Заметки"
                                    iconName="notes"
                                    icon={TabIcon}
                             >
                                 <Scene key='Notes' title='Заметки' component={Notes}
                                        titleStyle={style.stackTitle}
                                        renderLeftButton={ ()=>this._leftButton()}
                                        renderRightButton={ ()=>this._rightButton()}/>
                                 <Scene key='NewNote' title='Новая заметка' component={NewNote}
                                        tintColor="#fff"
                                        titleStyle={style.stackTitle}
                                        renderRightButton={ ()=>this._rightButton()}/>
                             </Scene>
                             <Scene key="StackFinance" title="Финансы"
                                    iconName="coins"
                                    icon={TabIcon}>
                                 <Scene key='Finance' title='Финансы' component={Finance}
                                        titleStyle={style.stackTitle}
                                        renderLeftButton={ ()=>this._leftButton()}
                                        renderRightButton={ ()=>this._rightButton()}/>
                                 <Scene key='NewPayment' title='Новый расход' component={NewPayment}
                                        tintColor="#fff"
                                        titleStyle={style.stackTitle}
                                        renderRightButton={ ()=>this._rightButton()}/>
                                 <Scene key='AllFinance' title='Расходы за месяц' component={AllFinance}
                                        tintColor="#fff"
                                        titleStyle={style.stackTitle}
                                        renderRightButton={ ()=>this._rightButton()}/>
                             </Scene>
                             <Scene key="StackTheDoctors" title="Доктора" iconName="doctor" icon={TabIcon}>
                                 <Scene key='TheDoctors' title='Доктора' component={TheDoctors}
                                        titleStyle={style.stackTitle}
                                        renderLeftButton={ ()=>this._leftButton()}
                                        renderRightButton={ ()=>this._rightButton()}/>
                                 <Scene key='NewDoctor' title='Добавление доктора' component={NewDoctor}
                                        tintColor="#fff"
                                        titleStyle={style.stackTitle}
                                        renderRightButton={ ()=>this._rightButton()}/>
                             </Scene>
                             <Scene key='Pills' title='Таблетки' component={Pills}
                                    titleStyle={style.stackTitle}
                                    iconName="pills"
                                    icon={TabIcon}
                                    renderLeftButton={ ()=>this._leftButton()}
                                    renderRightButton={ ()=>this._rightButton()}/>
                         </Scene>
                         <Scene key='Profile' title='Личный кабинет' component={Profile}
                                tintColor="#fff"
                                hideNavBar={false}
                                 {...style}
                                titleStyle={style.stackTitle}
                                renderRightButton={ ()=>this._rightButton()}/>
                         <Scene key='Notifications' title='Оповещение' component={Notifications}
                                tintColor="#fff"
                                renderLeftButton={<View/>}
                                hideNavBar={false}
                                renderRightButton={ ()=>this._rightButtonClose()}
                                {...style}
                                titleStyle={style.stackTitle}/>
                         <Scene key='EditProfile' title='Редактировать' component={EditProfile}
                                tintColor="#fff"
                                hideNavBar={false}
                                {...style}
                                titleStyle={style.stackTitle}
                                renderRightButton={ ()=>this._rightButton()}/>
                     </Scene>
                 </Router>
             </Provider>
     );
  }
}
const style = StyleSheet.create({
    navigationBarStyle : {
        backgroundColor: config.interfaceAppColor,
        borderBottomLeftRadius:15,
        borderBottomRightRadius:15
    },
    tabBarStyle:{
        backgroundColor:config.interfaceAppColor,
        borderTopLeftRadius:15,
        borderTopRightRadius:15
    },
    titleStyle: {
        marginLeft: 75,
        color: '#ffffff',
        fontSize: 20,
        alignSelf: 'center'
    },
    stackTitle:{
        marginLeft: 15,
        color: '#ffffff',
        fontSize: 20,
        alignSelf: 'center'
    }
});
