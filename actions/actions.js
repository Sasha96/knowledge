import {mainReducerActionTypes} from '../reducers/mainReducer';
import {store} from '../index';

export function changeStatus() {
    store.dispatch({
        type: mainReducerActionTypes.FIRST_ENTER,
        payload:'false'
    });
}
export function newNotification(value) {
    store.dispatch({
        type: mainReducerActionTypes.GET_NEW_NOTIFICATION,
        payload:value
    });
}
export function syncNotifications(value) {
    console.log('syncNotifications',value)
    store.dispatch({
        type: mainReducerActionTypes.NOTIFICATION_ADD,
        payload:value
    });
}