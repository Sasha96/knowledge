export const mainReducerActionTypes = {
    NOTIFICATION_ADD: 'NOTIFICATION_ADD',
    FIRST_ENTER:'FIRST_ENTER',
    GET_NEW_NOTIFICATION:'GET_NEW_NOTIFICATION',
    GET_UID:'GET_UID'
};
const initialState = {
    notification:[],
    firstEnter:'true',
    getNewNotification:'false',
    uid:''
}
export default function mainReducer(state = initialState, action){
    if(action.type === mainReducerActionTypes.GET_UID) {
        return{
            ...state,
            uid: action.payload,
        }
    }
    if(action.type === mainReducerActionTypes.GET_NEW_NOTIFICATION) {
        return{
            ...state,
            getNewNotification: action.payload,
        }
    }
    if(action.type === mainReducerActionTypes.FIRST_ENTER) {
        return{
            ...state,
            firstEnter: action.payload,
        }
    }
    if(action.type === mainReducerActionTypes.NOTIFICATION_ADD) {
        return{
            ...state,
            notification: action.payload,
        }
    }
    return state;
}