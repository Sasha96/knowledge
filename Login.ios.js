import React, { Component } from 'react';

import { connect } from 'react-redux';

import {View, Text,Image,Keyboard,Alert,
    Dimensions,AsyncStorage,TouchableOpacity,
    ToastAndroid} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { BarIndicator} from 'react-native-indicators';
import {fbApp} from '../index';
import {styles,config} from '../config';
import firebase from 'react-native-firebase';

class Login extends Component{
    constructor(){
        super();
        this.state={
            email:'',
            password:'',
            keyboard:false,
            loading:false,
            user: null
        }
    }
    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    _keyboardDidShow = () => this.setState({keyboard:true})
    _keyboardDidHide = () => this.setState({keyboard:false});
    validateEmail() {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(this.state.email).toLowerCase());
    }
    validatePassword(){
        if(this.state.password.length<6) return false;
        return true;
    }
    onLogin = () => {
        if(!this.validateEmail())return Alert.alert('Error','Не правильно указан Email');
        if(!this.validatePassword())return Alert.alert('Error','Пароль должен быть больше 6 символов');
        this.setState({loading:true})
        const { email, password } = this.state;
        fbApp.auth().signInWithEmailAndPassword(email, password)
            .then(async(user) => {
                await AsyncStorage.setItem('uid', user.uid);
                await AsyncStorage.setItem('auth', 'true');
                await fbApp.messaging().getToken().then(
                    async (token) => {
                        console.log(token)
                        await fbApp.database().ref('users').child(user.uid).child('notificationTokens').push({token:token});
                    }
                );
                Actions.Main();
                this.setState({loading:false})
            })
            .catch((error) => {
                const { code, message } = error;
                console.log('error',code)
                if(code==='auth/user-not-found') Alert.alert('Не верный логин или пароль');
                this.setState({loading:false})
            });
    }
    onLoginOrRegisterFacebook = () => {
        Alert.alert('В разработке');
    }
    onLoginOrRegisterGoogle = () => {
        Alert.alert('В разработке');
    };
    render() {
        return (
            <View style={[this.state.keyboard?styles.keyboardTrue:styles.centerBlock,{backgroundColor:config.backgroundAppColor}]}>
                {this.state.loading?
                    <View style={styles.loadingView}>
                       <BarIndicator color='#279ef4' size={100}/>
                    </View> :null}
                    <View style={{flex:1,flexDirection:'column'}}>
                    <View style={{alignItems:'center'}}>
                        <Image style={{height: 150, width:150}} source={require('../images/logo.png')}/>
                    </View>
                    <Form>
                        <Item>
                            <Input
                                style={styles.IOSTextInput}
                                ref="email"
                                returnKeyType='next'
                                onSubmitEditing={() => this.refs.password._root.focus()}
                                autoCapitalize="none"
                                keyboardType="email-address"
                                placeholder="Email"
                                value={this.state.email}
                                onChangeText={(text)=>this.setState({email:text})}
                            />
                        </Item>
                        <Item>
                            <Input
                                style={styles.IOSTextInput}
                                returnKeyType='done'
                                ref="password"
                                onSubmitEditing={() => this.onLogin()}
                                secureTextEntry
                                autoCapitalize="none"
                                placeholder="Пароль"
                                value={this.state.password}
                                onChangeText={(text)=>this.setState({password:text})}
                            />
                        </Item>

                        <View style={{flexDirection:'row',justifyContent:'center',margin:10}}>
                            <TouchableOpacity onPress={()=>Actions.forgotpassword()}>
                                <Text style={{padding:5}}>Забыли пароль?</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <Button style={styles.marginButton} block rounded  onPress={()=>this.onLogin()}>
                                <Text style={styles.textButtonColor}>Вход</Text>
                            </Button>
                            <Button style={styles.marginButton} block rounded onPress={()=>Actions.registration()}>
                                <Text style={styles.textButtonColor}>Регистрация</Text>
                            </Button>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <Text>или войти через</Text>
                        </View>
                        <View style={{justifyContent:'center',flexDirection:'row',paddingHorizontal:30,marginTop:10}}>
                            <TouchableOpacity onPress={()=>this.onLoginOrRegisterGoogle()}>
                                <Image style={{height: 60, marginHorizontal:5}}
                                       resizeMode="contain"
                                       source={require('../images/googleIcon.png')}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>this.onLoginOrRegisterFacebook()}>
                                <Image style={{height: 60,marginHorizontal:5}}
                                       resizeMode="contain"
                                       source={require('../images/facebookIcon.png')}/>
                            </TouchableOpacity>
                        </View>
                    </Form>
                    </View>
            </View>
        );
    }

}

export default connect(
    state => ({
        state:state
    })
)(Login);
